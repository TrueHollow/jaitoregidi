'use strict';

const log4js = require('log4js');
const config = require('config');

// Get configuration, for current environment
log4js.configure(config.get("log4js"));

const logger = log4js.getLogger();

module.exports = logger;
'use strict';

const fs = require('fs');
// Logger
const logger = require('./Logger');

const stringify = require('csv-stringify');

class File {
    static SaveJsonData(data, fileName = 'result.json') {
        logger.info('Saving result to file.');
        return new Promise((resolve, reject) => {
            const json = JSON.stringify(data, null, '\t');
            fs.writeFile(`./${fileName}`, json, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    static SaveCsvData(data, fileName = 'result.csv') {
        return new Promise((resolve, reject) => {
            stringify(data, {
                header: true,
                delimiter: ';'
            }, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            })
        }).then((csv) => {
            return new Promise((resolve, reject) => {
                fs.writeFile(`./${fileName}`, csv, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            });
        });
    }
}

module.exports = File;
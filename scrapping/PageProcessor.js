'use strict';

const logger = require('../common/Logger');

const getRandom = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
};

async function autoScroll(page){
    logger.info('PageProcessor: scrolling to bottom to load all images');
    await page.evaluate(async () => {
        await new Promise((resolve) => {
            let totalHeight = 0;
            const distance = 100;
            const timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if(totalHeight >= scrollHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}

async function mouseMoving(page) {
    logger.info('PageProcessor: moving mouse (emulate user input)');
    const limit = getRandom(3, 6);
    for (let i = 0; i < limit; ++i) {
        const x = getRandom(50, 200);
        const y = getRandom(50, 200);
        logger.info('PageProcessor mouseMoving (x: %d) (y: %d)', x, y);
        await page.mouse.move(x, y);
    }
}

class PageProcessor {
    constructor(page, options) {
        logger.info('PageProcessor created');
        this.page = page;
        this.options = options;
        this.error = null;
    }

    async linkToNextPage() {
        return this.page.evaluate(() => {
            const content = document.querySelector('.next');
            if (!content) return;
            const aElements = content.getElementsByTagName('a');
            if (aElements.length !== 1) return;
            const aEl = aElements[0];
            const href = aEl.getAttribute('href');
            if (!href) return;
            return new URL(href, window.location).toString();
        });
    }

    async getAdsOnPage() {
        return this.page.evaluate(() => {
            const NumberInt = (str) => {
                return Number.parseInt(str.replace(',', '').replace('.', ''));
            };
            const result = [];
            const content = document.querySelector('.items-container');
            if (content) {
                const ads = content.getElementsByTagName('article');
                for (let ad of ads) {
                    if (ad.classList.contains('adv')) {
                        // Useless adv
                        continue;
                    }
                    let AdId = ad.getAttribute('data-adid');
                    if (AdId) {
                        AdId = NumberInt(AdId);
                    } else {
                        // Something wrong.
                        console.error('AdId is not found for element. Markup is changed?');
                        console.error(ad);
                        continue;
                    }
                    const aTag = ad.querySelector('.item-link ');
                    const Title = (aTag) ? aTag.getAttribute('title') : '';
                    let Link = (aTag) ? aTag.getAttribute('href') : '';
                    if (Link) {
                        Link = new URL(Link, window.location).toString();
                    }
                    let Price = (ad.querySelector('.price-row span')) ? ad.querySelector('.price-row span').textContent : '';
                    if (Price) {
                        Price = NumberInt(Price);
                    }

                    let Size = null;
                    let PriceM = null;
                    const details = ad.getElementsByClassName('item-detail');
                    if (details.length === 3) {
                        Size = NumberInt(details[1].textContent);
                        // PriceM = NumberInt(details[1].textContent);
                    }

                    const Description = (ad.querySelector('.item-description')) ? ad.querySelector('.item-description').textContent.trim() : '';
                    const ImageUrl = (ad.querySelector('.item-gallery img')) ? ad.querySelector('.item-gallery img').src : '';

                    result.push({
                        AdId: AdId,
                        Title: Title,
                        Price: Price,
                        Link: Link,
                        Size: Size,
                        PriceM: PriceM,
                        Description: Description,
                        ImageUrl: ImageUrl
                    });
                }
            }
            return result;
        });
    }

    async getLocations(arr, url) {
        let text;
        this.page.once('response', async (response) => {
            text = await response.text();
        });
        await this.page.goto(url, {waitUntil: 'networkidle0'});
        let jsonData;
        try {
            jsonData = JSON.parse(text);
        } catch (e) {
            logger.error('Cannot parse locations json: %s', e);
        }
        if (jsonData) {
            const items = jsonData.jsonResponse.map.items;
            arr = arr.map(dataObject => {
                for (let item of items) {
                    if (dataObject.AdId === item.adId) {
                        dataObject.latitude = item.latitude;
                        dataObject.longitude = item.longitude;
                        break;
                    }
                }
                return dataObject;
            });
        }
        return arr;
    }

    async isCaptcha() {
        return this.page.evaluate(() => {
            return document.querySelector('.g-recaptcha');
        });
    }

    async getAds(url) {
        let result = [];
        let nextPageLink = url;
        do {
            this.lastpage = nextPageLink;
            logger.info('PageProcessor: goto (%s)', nextPageLink);
            await mouseMoving(this.page);
            await this.page.goto(nextPageLink, {waitUntil: 'networkidle0'});
            await mouseMoving(this.page);
            // Scrolling to download all ads images
            await autoScroll(this.page);
            // waiting more for images
            await this.delay(5000);
            result = result.concat(await this.getAdsOnPage());
            await this.delay();
            nextPageLink = await this.linkToNextPage();
            if (!nextPageLink) {
                await this.page.screenshot({ path: "logs/lastpage.png", fullPage: true });
                if (await this.isCaptcha()) {
                    logger.warn('PageProcessor: captcha found.');
                    this.error = true;
                }
                break;
            }
        } while (true);
        await this.page.close();
        return result;
    }

    async delay(value) {
       return new Promise(resolve => {
           const rndDelay = (value) ? value : getRandom(this.options.minDelay, this.options.maxDelay);
           logger.info('PageProcessor: delay (%d)', rndDelay);
           setTimeout(resolve, rndDelay);
       })
    }
}

module.exports = PageProcessor;

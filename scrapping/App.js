'use strict';

// Logger
const logger = require('../common/Logger');

logger.info('App: script invoked');

const puppeteer = require("puppeteer-extra");

const pluginStealth = require("puppeteer-extra-plugin-stealth");
puppeteer.use(pluginStealth());

const config = require('config');

class Browser {
    constructor(configuration) {
        this.configuration = configuration;
        this.browser = null;
    }

    async create() {
        this.browser = await puppeteer.launch(this.configuration);
    }

    async getNewPage() {
        if (this.browser) {
            const page = await this.browser.newPage();
            page.on('console', msg =>
                logger.debug('PAGE LOG:', msg.type(), msg.text())
            );
            await page.setViewport({width: 1920, height: 1040});
            return page;
        }
    }

    async close() {
        if (this.browser) {
            await this.browser.close();
        }
    }
}

const PageProcessor = require('./PageProcessor');

const appendNewValues = (destinationArray, newArray) => {
    for (let newEl of newArray) {
        let found = false;
        for (let oldEl of destinationArray) {
            if (oldEl.AdId === newEl.AdId) {
                found = true;
                break;
            }
        }
        if (!found) {
            destinationArray.push(newEl);
        }
    }
    return destinationArray;
};

// Running parsing code
const getPlaces = async (jsonObject) => {
    logger.info('App: Running getPlaces code');
    let fullResult = [];
    do {
        let browser;
        let stop = false;
        let processor;
        try {
            browser = new Browser(config.get('chrome'));
            await browser.create();
            const page = await browser.getNewPage();
            processor = new PageProcessor(page, jsonObject);
            const result = await processor.getAds(jsonObject.url);
            fullResult = appendNewValues(fullResult, result);
            if (processor.error) {
                jsonObject.url = processor.lastpage;
            } else {
                stop = true;
            }
        } catch (e) {
            logger.error(e);
        }
        if (processor && processor.lastpage) {
            jsonObject.url = processor.lastpage;
        }
        if (browser) {
            await browser.close();
        }
        if (stop) {
            break;
        }
    } while (true);
    return fullResult;
};

const setLocationToPlaces = async (arr, url) => {
    logger.info('App: Running setLocationToPlaces code');
    let fullResult = arr;
    let browser;
    try {
        browser = new Browser(config.get('chrome'));
        await browser.create();
        const page = await browser.getNewPage();
        const processor = new PageProcessor(page);
        fullResult = await processor.getLocations(arr, url);
    } catch (e) {
        logger.error(e);
    }
    if (browser) {
        browser.close();
    }
    return fullResult;
};

// Invoke App.run()
// jsonObject - options to
const run = async (jsonObject) => {
    logger.info('App: Invoke App.run()');
    const rawResult = await getPlaces(jsonObject);
    return await setLocationToPlaces(rawResult, jsonObject.urlLocations);
};

module.exports = {
    run: run
};

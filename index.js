'use strict';

const config = require('config');
const logger = require('./common/Logger');

logger.info('Script started');

const File = require('./common/File');
const App = require('./scrapping/App');

const scrappingOptions = config.util.toObject(config.get('Scrapping'));
App.run(scrappingOptions)
//Promise.resolve(require('./result'))
    .then(async (result) => {
        //await File.SaveJsonData(result);
        await File.SaveCsvData(result);
        logger.info('Script is finished.');
    }).catch(e => {
    logger.error('Unhandled error occured.');
    logger.error(e);
    throw e;
});
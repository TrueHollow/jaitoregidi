## Preparation

### Create configuration file

App now using [config package](https://www.npmjs.com/package/config) to manage configuration. 

Create custom configuration file `custom_file.json` (for example) with

```json
{
  "chrome": {
    "args": [
      "--no-sandbox",
      ",--disable-setuid-sandbox"
    ],
    "headless": false
  },
  "log4js": {
    "appenders": {
      "std_out": {
        "type": "stdout"
      },
      "file_log": {
        "type": "file",
        "filename": "logs/all.log",
        "maxLogSize": 10485760,
        "backups": 10,
        "compress": true
      }
    },
    "categories": {
      "default": {
        "appenders": [
          "std_out",
          "file_log"
        ],
        "level": "info"
      }
    }
  },
  "Scrapping": {
    "url": "https://www.idealista.com/areas/venta-locales/con-precio-hasta_200000,metros-cuadrados-mas-de_60/?shape=%28%28o%7DvgGrdqQg%7DDuoH%60%7ECapJfoBsGfEv_Cmd%40d%7EEhvEtI%60_Gy%60I%7C%7CGswPniFdzG%7BcC%7E%7CJagEnwDklBtaBw%7CAjfD__Cm%7BBorB%60dCceGxtA%29%29"
  }
}
```

Update environment variable `NODE_ENV` (for example):

```sh
export NODE_ENV=custom_file
```

### Install dependencies

Run command `npm install`

## Running app

Run command `npm start`